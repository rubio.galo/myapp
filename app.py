import json
from flask import Flask, Response, make_response, render_template, request, jsonify, redirect
import generate
import pandas as pd

app = Flask(__name__)

@app.route('/', methods=['GET'])
def say_name():
    return render_template('simple.html')

@app.route('/all', methods=['GET'])
def getAll():
    df = generate.geretateAll()
    resp = make_response(df.to_csv())
    resp.headers["Content-Disposition"] = "attachment; filename=export.csv"
    resp.headers["Content-Type"] = "text/csv"
    return resp
    
@app.route('/data', methods=("POST", "GET"))
def html_table():
    df = pd.read_csv('static/out.csv')
    return render_template('data.html',  tables=[df.to_html(classes='table table-hover', table_id='tblData')], titles=df.columns.values)

@app.route('/api/csv', methods=['POST'])
def get_csv():
    json = request.get_json()
    mesIni = json["mesIni"]
    mesFin = json["mesFin"]+1
    anioIni = json["anioIni"]
    anioFin = json["anioFin"]
    df = generate.geretate(mesIni,anioIni,mesFin, anioFin)
    return Response(df.tail(n=100).to_json(orient="records"), mimetype='application/json')

if __name__ == '__main__':
    app.run(debug=True, port=80)
