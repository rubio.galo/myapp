from sqlalchemy import create_engine, true
import pandas as pd
import os
import numpy as np

def geretate(mesIni, anioIni, mesFin, anioFin):
    
    dbUser = os.environ.get('DBUSER')
    dbPassword = os.environ.get('DBPASSWORD')
    dbHost = os.environ.get('DBHOST')
    dbPort = os.environ.get('DBPORT')
    dbName = os.environ.get('DBNAME')
    
    #db_connection_str = 'mysql://%s:%s@%s/%s' % (dbUser, dbPassword, dbHost, dbName)
    db_connection_str = 'mysql://root:T3lc0n3t.2021@172.30.147.69/zabbixalerts'
    db_connection = create_engine(db_connection_str)

    vms = pd.read_sql(
        '''SELECT DISTINCT
            vm.name,
            i.vcpu,
            i.memory,
            i.disk,
            vm.powered,
            vm.city,
            vm.vcenter,
            vm.razon_social,
            vm.login,
            vm.area,
            CAST(i.id_usememory AS INT) AS id_usememory,
            CAST(i.id_usecpu AS INT) AS id_usecpu 
        FROM Vm_zabbix vm 
        INNER JOIN Vm_items i ON vm.uniqueid = i.uniqueid_items 
        WHERE vm.status = 'monitored host';''', con=db_connection)

    sqlQuery = '''SELECT 
            CAST(t.itemid AS INT) AS itemid, 
            TRUNCATE(MIN(t.value_min),2), 
            TRUNCATE(MAX(t.value_max),2), 
            TRUNCATE(AVG(t.value_avg),2), 
            MONTH(FROM_UNIXTIME(t.clock)) AS mes, 
            YEAR(FROM_UNIXTIME(t.clock)) AS anio 
            FROM Vm_trends t 
            WHERE FROM_UNIXTIME(t.clock) >= '{anioI}-{mesI}-01 00:00:00' AND FROM_UNIXTIME(t.clock) < '{anioF}-{mesF}-01 00:00:00'
            GROUP BY t.itemid, mes, anio'''.format(anioI=anioIni, mesI=mesIni, anioF=anioFin, mesF=mesFin)
    trends = pd.read_sql(sqlQuery , con=db_connection)

    df = vms.set_index('id_usememory').join(trends.set_index('itemid'))
    df.rename(columns = {'TRUNCATE(MIN(t.value_min),2)':'memory_min', 'TRUNCATE(MAX(t.value_max),2)':'memory_max', 'TRUNCATE(AVG(t.value_avg),2)':'memory_avg'}, inplace = True)

    df2 = vms.set_index('id_usecpu').join(trends.set_index('itemid'))
    df2.rename(columns = {'TRUNCATE(MIN(t.value_min),2)':'cpu_min', 'TRUNCATE(MAX(t.value_max),2)':'cpu_max', 'TRUNCATE(AVG(t.value_avg),2)':'cpu_avg'}, inplace = True)

    new_df = pd.merge(df, df2,  how='left', left_on=['name', 'powered', 'vcpu','memory','disk','city','vcenter','razon_social','login','area', 'mes', 'anio'], right_on = ['name', 'powered', 'vcpu','memory','disk','city','vcenter','razon_social','login','area', 'mes', 'anio'])
    new_df = new_df[['name', 'powered', 'vcpu', 'memory', 'disk', 'city', 'vcenter', 'razon_social', 'login', 'area', 'memory_min', 'memory_max', 'memory_avg', 'cpu_min', 'cpu_max', 'cpu_avg', 'mes', 'anio',]]
    new_df.loc[new_df['memory_max'] > 75, 'mem_rec'] = new_df['memory'].astype(float)
    new_df.loc[new_df['memory_max'] < 75, 'mem_rec'] = np.ceil((new_df['memory_max'] + 25) * new_df['memory'] /100)
    new_df.loc[new_df['cpu_max'] > 75, 'cpu_rec'] = new_df['vcpu'].astype(float)
    new_df.loc[new_df['cpu_max'] < 75, 'cpu_rec'] = np.ceil((new_df['cpu_max'] + 25) * new_df['vcpu'] /100)
    new_df.to_csv('./static/out.csv')
    return new_df

def geretateAll():
    
    dbUser = os.environ.get('DBUSER')
    dbPassword = os.environ.get('DBPASSWORD')
    dbHost = os.environ.get('DBHOST')
    dbPort = os.environ.get('DBPORT')
    dbName = os.environ.get('DBNAME')
    
    #db_connection_str = 'mysql://%s:%s@%s/%s' % (dbUser, dbPassword, dbHost, dbName)
    db_connection_str = 'mysql://root:T3lc0n3t.2021@172.30.147.69/zabbixalerts'
    db_connection = create_engine(db_connection_str)

    vms = pd.read_sql(
        '''SELECT DISTINCT
            vm.name,
            i.vcpu,
            i.memory,
            i.disk,
            vm.powered,
            vm.city,
            vm.vcenter,
            vm.razon_social,
            vm.login,
            vm.area,
            CAST(i.id_usememory AS INT) AS id_usememory,
            CAST(i.id_usecpu AS INT) AS id_usecpu 
        FROM Vm_zabbix vm 
        INNER JOIN Vm_items i ON vm.uniqueid = i.uniqueid_items
        WHERE vm.status = 'monitored host';''', con=db_connection)

    trends = pd.read_sql(
        '''SELECT 
            CAST(t.itemid AS INT) AS itemid, 
            TRUNCATE(MIN(t.value_min),2), 
            TRUNCATE(MAX(t.value_max),2), 
            TRUNCATE(AVG(t.value_avg),2), 
            MONTH(FROM_UNIXTIME(t.clock)) AS mes, 
            YEAR(FROM_UNIXTIME(t.clock)) AS anio 
            FROM Vm_trends t 
            GROUP BY t.itemid, mes, anio''', con=db_connection)

    df = vms.set_index('id_usememory').join(trends.set_index('itemid'))
    df.rename(columns = {'TRUNCATE(MIN(t.value_min),2)':'memory_min', 'TRUNCATE(MAX(t.value_max),2)':'memory_max', 'TRUNCATE(AVG(t.value_avg),2)':'memory_avg'}, inplace = True)

    df2 = vms.set_index('id_usecpu').join(trends.set_index('itemid'))
    df2.rename(columns = {'TRUNCATE(MIN(t.value_min),2)':'memory_min', 'TRUNCATE(MAX(t.value_max),2)':'memory_max', 'TRUNCATE(AVG(t.value_avg),2)':'memory_avg'}, inplace = True)

    new_df = pd.merge(df, df2,  how='left', left_on=['name', 'powered', 'vcpu','memory','disk','city','vcenter','razon_social','login','area', 'mes', 'anio'], right_on = ['name', 'powered', 'vcpu','memory','disk','city','vcenter','razon_social','login','area', 'mes', 'anio'])
    new_df = new_df[['name', 'powered', 'vcpu', 'memory', 'disk', 'city', 'vcenter', 'razon_social', 'login', 'area', 'memory_min', 'memory_max', 'memory_avg', 'cpu_min', 'cpu_max', 'cpu_avg', 'mes', 'anio',]]
    new_df.loc[new_df['memory_max'] > 75, 'mem_rec'] = new_df['memory'].astype(float)
    new_df.loc[new_df['memory_max'] < 75, 'mem_rec'] = np.ceil((new_df['memory_max'] + 25) * new_df['memory'] /100)
    new_df.loc[new_df['cpu_max'] > 75, 'cpu_rec'] = new_df['vcpu'].astype(float)
    new_df.loc[new_df['cpu_max'] < 75, 'cpu_rec'] = np.ceil((new_df['cpu_max'] + 25) * new_df['vcpu'] /100)
    new_df.to_csv('./static/out.csv')    
    return new_df