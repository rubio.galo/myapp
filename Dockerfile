FROM python:3

WORKDIR /app

RUN ln -sf /usr/share/zoneinfo/America/Guayaquil /etc/localtime

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY * ./
RUN mkdir static templates
RUN mkdir static/assets
COPY ./static/* ./static
COPY ./templates/* ./templates
COPY ./static/assets/* ./static/assets

EXPOSE 80

CMD [ "./start.sh" ]
# CMD [ "gunicorn", "--bind 0.0.0.0:80", "wsgi:app" ]
# CMD [ "/bin/bash" ]
